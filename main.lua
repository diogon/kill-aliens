--
-- Created by IntelliJ IDEA.
-- Author: diogON.com
-- Date: 28/09/14
-- Time: 11:59
-- To change this template use File | Settings | File Templates.
--

-- Functions

local Main = {}
local startButtonListeners = {}
local showCredits = {}
local hideCredits = {}
local showGameView = {}
local gameListeners = {}
local createEnemy = {}
local shoot = {}
--[[local update = {}]]
local onCollision = {}
local addExBullets = {}
local alert = {}


-- Iniciando a f�sica do jogo
local physics = require("physics")
physics.start()
system.activate ("multitouch")
--physics.setDrawMode("hybrid")
physics.setGravity( .5, .1 )



-- Pegando as dimens�es da tela(Display)
local w = display.contentWidth
local h = display.contentHeight


-- Criando as Paredes do jogo
local leftWall = display.newRect(0,0,1,h)
local rightWall = display.newRect(w,0,1,h)

-- Tornando a parede corpos f�sicos

physics.addBody(leftWall,"static",{bounce =0.5})
physics.addBody(rightWall,"static",{bounce =0.5})



local options = { frames = require("meteorys").frames }


-- Background Options
local screen_adjustment = 1
local background = display.newImage("./back/space.jpg")
display.setStatusBar(display.HiddenStatusBar)
background:scale( .5, .5)

background.x = display.contentCenterX
background.y = display.contentCenterY


-- [Title View(Intro do jogo)]

local title
local start
local online
local titleView

-- [CreditsView]

local creditsView

-- Instructions

local ins

-- Alert Dialogos que v�o aparece rno jogo

local alertView

-- Som do jogo
local laserSound = audio.loadSound( "./media/start.mp3" )
local moveSound = audio.loadSound( "./media/teste1.mp3",{ channel=2 } )
audio.setVolume( 0.2, { channel=1 } )
audio.setVolume( 1, { channel=2 } )






-- Inicio do jogo(Intro Primeira tela)

function Main()
    titleBg = display.newImage('./media/title/elipse.png',20,30)

    blueBg = display.newImage('./media/title/bluebg.png', 35,48)
    greyBg = display.newImage('./media/title/greybg.png', 40,43)
    logoBg = display.newImage('./media/title/logobg.png', 35,43)
    textoBg = display.newImage('./media/title/textobg.png', 35,40)
    start = display.newImage('./media/title/start.png',35,display.stageHeight /1.5)
    scores = display.newImage('./media/title/scores.png',display.stageWidth /40,display.stageHeight /1.34)
    online = display.newImage('./media/title/online.png',display.stageWidth /5,display.stageHeight /1.2)

    online:scale(1.2,1.2)
    scores:scale(.8,.8)


    --[[playBtn = display.newImage('./media/title/bluebg.png', 40,50)]]
   --[[ creditsBtn = display.newImage('creditsBtn.png', 191, 223)]]
    titleView = display.newGroup(titleBg,greyBg,blueBg,logoBg,textoBg,start,scores,online)

    startButtonListeners('add')

--[[-- Fun��o de anima��o na intro do jogo
    local function animate(event)

    titleBg.rotation = titleBg.rotation +.1
    blueBg.rotation = blueBg.rotation -.1


    end
    Runtime:addEventListener("enterFrame",animate)
    ]]--[[startButtonListeners('add')]]
end


-- Aguardando o jogando entrar jogo (Start)

function startButtonListeners(action)
    if(action == 'add') then
        start:addEventListener('tap', showGameView)
        online:addEventListener('tap', showCredits)
    else
        start:removeEventListener('tap', showGameView)
        online:removeEventListener('tap', showCredits)
    end
end


-- Mostrar Creditos

function showCredits:tap(e)
    start.isVisible = false
    online.isVisible = false
    titleView.isVisible = false
    creditsView = display.newImage('./media/online/comingsoon.png')
    transition.to(creditsView, {time = 300, x = 200, onComplete = function() creditsView:addEventListener('tap', hideCredits) end})
end


-- Esconder Cr�ditos

function hideCredits:tap(e)
    playBtn.isVisible = true
    creditsBtn.isVisible = true
    transition.to(creditsView, {time = 300, y = display.contentHeight+creditsView.height, onComplete = function() creditsView:removeEventListener('tap', hideCredits) display.remove(creditsView) creditsView = nil end } )
end


--Come�ar partida

function showGameView:tap(e)
    Runtime:removeEventListener( "enterFrame", onFrame )
    audio.play(laserSound,{ channel=1 })

    transition.to(titleView, {time = 500, x = -titleView.height, onComplete = function() startButtonListeners('rmv') display.remove(titleView) titleView = nil end})


    --Nave do Jogo
    local sheetData =  { width=41, height=43, numFrames=9 }
    -- width: largura de cada frame
    -- height: altura de cada frame
    -- numFrames: n�mero de frames da sprite

    local sheet = graphics.newImageSheet("./char/nave2.png", sheetData)
    -- cria uma nova imagem usando a sprite "gaara.png" e as propriedades vistas acima

    local sequenceData =
    {
        { name = "idleAway", start = 5, count = 1, time = 0, loopCount = 1 }, --idleDown = parado para baixo (� s� um nome, vc quem nomeia como quiser)
        -- name: nome desse movimento
        -- start: frame da sprite onde a anima��o come�a (nesse caso come�a no primeiro frame da sprite)
        -- count: n�mero de frames para essa anima��o (nesse caso essa anima��o s� ter� um frame, o primeiro, aquele q o gaara t� parado pra baixo)
        -- time: tempo de dura��o da anima��o (est� zero pq essa anima��o s� tem um quadro
        -- loopCount: n�mero de vezes que a anima��o � executada, nesse caso a anima��o s� � executada uma vez, pois s� tem um frame


        { name = "moveLeft", start = 4, count = 1, time = 0, loopCount = 1 },
        -- mesmo principio da anima��o de cima, mas agora come�a no quinto frame (start=5) e cont�m 2 frames de anima��o apenas
        { name = "moveRight", start = 6, count = 1, time = 0, loopCount = 1 },

    }
	
	local function clearMeteory( thisMeteory )
		display.remove(thisMeteory) ; 
		thisMeteory = nil
	end
	
	local function spawnMeteorys()

		local meteory = display.newImageRect("./enemy/meteory2.png",45,45);
		-- smile:setReferencePoint(display.CenterReferencePoint);  --not necessary; center is default
		meteory.x = math.random(-10, 400);
		meteory.y = -40;
		transition.to( meteory, { time=math.random(2000,8000), x=math.random(-10,400) , y=600, onComplete=clearMeteory } );
		physics.addBody( meteory, "dynamic", { density=0.1, bounce=0.1, friction=0.1, radius=0 } );

	end

	timer.performWithDelay( 500, spawnMeteorys, 0 )  --fire every 10 seconds
	
    local player = display.newSprite(sheet, sequenceData)
    -- cria finalmente a sprite utilizando as propriedades vistas acima

    player.x = w *.5
    player.y = h *.7

    --define the shape table (once created, this can be used multiple times)
    local triangleShape = { 0,-25, 25,10, -25,10 }

    physics.addBody( player, { shape=triangleShape, density=1.6, friction=0.5, bounce=0.2 } )

    player.gravityScale = 0


    local function fireLasers(event)
		local blaster = display.newImageRect("./media/laser.png", 8,24)
		blaster.x = player.x
		blaster.y = player.y - 30
  
		local function removeLaser(target)  -- `onComplete` sends object as a parameter
			target:removeSelf()
			target = nil
		end

		transition.to(blaster,{time=1000,y=0, onComplete = removeLaser})

	end
	
	local fireTimer
	
	local function onTouch(event)
	
		local touchedObject = event.target
	
		if event.phase=="began" then
			touchedObject.previousX = touchedObject.x
			touchedObject.previousY = touchedObject.y
			fireTimer = timer.performWithDelay( 180, fireLasers, 0 )
		elseif event.phase=="moved" then
			touchedObject.x = (event.x - event.xStart) + touchedObject.previousX
			touchedObject.y = (event.y - event.yStart) + touchedObject.previousY
		elseif ( event.phase == "ended" ) then
			-- stop firing the weapon
			timer.cancel( fireTimer )
		end
		
	end
	
	player:addEventListener("touch",onTouch)

end


Main()




--[[local largura = display.contentWidth
local altura = display.contentHeight

local arg = {}
arg[1] = 0
arg[2] = 255
arg[3] = 0


local retangulo = display.newRect(200,200,100,100)
retangulo.x = largura *.5
retangulo.y = altura *.5
retangulo:setFillColor(arg[1],arg[2],arg[3])


local function cor()

    local cont =1
    for cont =1,3,1 do
        arg[cont] = math.random(0, 255)
    end

    retangulo:setFillColor(arg[1],arg[2],arg[3])
    end

retangulo:addEventListener("tap",cor)]]

