local M = {}
M.sheetData = {
	frames = {
		{ name=meteory1, x = 0, y = 374, width = 82, height = 81, sourceX=0, sourceY=0, sourceWidth=82 , sourceHeight=81 },
		{ name=meteory2, x = 84, y = 374, width = 82, height = 81, sourceX=0, sourceY=0, sourceWidth=82 , sourceHeight=81 },
		{ name=meteory3, x = 0, y = 0, width = 187, height = 190, sourceX=0, sourceY=0, sourceWidth=187 , sourceHeight=190 },
		{ name=meteory4, x = 188, y = 192, width = 182, height = 180, sourceX=0, sourceY=0, sourceWidth=182 , sourceHeight=180 },
		{ name=meteory5, x = 188, y = 192, width = 182, height = 180, sourceX=0, sourceY=0, sourceWidth=182 , sourceHeight=180 },
		{ name=meteory6, x = 0, y = 192, width = 186, height = 169, sourceX=0, sourceY=0, sourceWidth=186 , sourceHeight=169 },
		{ name=meteory7, x = 0, y = 192, width = 186, height = 169, sourceX=0, sourceY=0, sourceWidth=186 , sourceHeight=169 }
	},
	sheetContentWidth = 512,
	sheetContentHeight = 512
}
return M